use std::error::Error;
use std::fs;
use std::io::{self, BufRead};
use std::path::Path;

fn main() -> Result<(), Box<dyn Error>> {
  println!("--- Part 1 ---");
  let commands = read_command_file("./res/input")?;
  let [horizontal, vertical] = simple_displacement(&commands);
  println!(
    "horizontal displacement: {}\n\
    vertical displacement: {}\n\
    horizontal displacement * vertical displacement: {}\n",
    horizontal,
    vertical,
    horizontal * vertical
  );
  println!("--- Part 2 ---");
  let [horizontal, vertical, aim] = aim_displacement(&commands);
  println!(
    "horizontal displacement: {}\n\
    vertical displacement: {}\n\
    final aim: {}\n\
    horizontal displacement * vertical displacement: {}\n",
    horizontal,
    vertical,
    aim,
    horizontal * vertical
  );
  Ok(())
}

#[derive(Clone, Copy, Debug)]
enum Command {
  Forward(isize),
  Down(isize),
  Up(isize),
}

impl Command {
  fn from_str<S: AsRef<str>>(s: S) -> Result<Self, io::Error> {
    let mut parts = s.as_ref().split(" ");
    if let Some(command_str) = parts.next() {
      if let Some(value_str) = parts.next() {
        let v: isize = value_str.parse().unwrap();
        return match command_str {
          "forward" => Ok(Command::Forward(v)),
          "down" => Ok(Command::Down(v)),
          "up" => Ok(Command::Up(v)),
          _ => Err(io::Error::new(
            io::ErrorKind::InvalidData,
            format!("Command not recognized: {}", command_str),
          )),
        };
      }
    }
    Err(io::Error::new(
      io::ErrorKind::InvalidData,
      format!("Command not parsed: {}", s.as_ref()),
    ))
  }
}

fn read_command_file<P: AsRef<Path>>(path: P) -> Result<Vec<Command>, io::Error> {
  let file = fs::File::open(path)?;
  Ok(
    io::BufReader::new(file)
      .lines()
      .filter_map(|line| Command::from_str(line.unwrap()).ok())
      .collect(),
  )
}

fn simple_displacement(commands: &Vec<Command>) -> [isize; 2] {
  let mut horizontal = 0isize;
  let mut vertical = 0isize;
  commands.iter().for_each(|command| match command {
    Command::Forward(v) => horizontal += v,
    Command::Up(v) => vertical -= v,
    Command::Down(v) => vertical += v,
  });
  [horizontal, vertical]
}

fn aim_displacement(commands: &Vec<Command>) -> [isize; 3] {
  let mut horizontal = 0isize;
  let mut vertical = 0isize;
  let mut aim = 0isize;
  commands.iter().for_each(|command| match command {
    Command::Forward(v) => {
      horizontal += v;
      vertical += aim * v;
    }
    Command::Up(v) => aim -= v,
    Command::Down(v) => aim += v,
  });
  [horizontal, vertical, aim]
}
